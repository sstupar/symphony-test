var webpackConfig = require('./webpack.test.js');

module.exports = config => {
  config.set({
    autoWatch: false,
    singleRun: true,
    browsers: [
      // 'Chrome', 
      'PhantomJS'
    ],
    basePath: '.',
    /*
     * list of files to load in the browser is built via spec-bundle.js
     */
    files: [
      'spec-bundle.js'
    ],
    exclude: [],
    frameworks: [
      'jasmine'      
    ],
    logLevel: config.LOG_INFO,
    phantomJsLauncher: {
      exitOnResourceError: true
    },
    port: 9876,
    colors: true,
    preprocessors: {
      'spec-bundle.js': ['webpack']
    },
    reporters: ['mocha'],
    webpack: webpackConfig,
    webpackServer: {
      noInfo: true
    }
  });
};