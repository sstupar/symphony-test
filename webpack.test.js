const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const path = require('path');
const webpack = require('webpack');

module.exports = {
  context: path.resolve(__dirname, 'src'),
  devtool: 'sourcemap',
  entry: {
    index: './index.js'
    // vendor: './vendor.js'
  },

  output: {
    path: path.join(__dirname, 'build'),
    filename: '[name].bundle.js'
  },
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /node_modules/,
      use: [{
        loader: 'babel-loader',
        options: {
          presets: [
            ['es2015', {
              modules: false
            }]
          ]
        }
      }]
      // , {
      //   loader: 'eslint-loader',
      //   options: {
      //     failOnWarning: false,
      //     failOnError: false
      //   }
      // }]
    }, {
      test: /\.html$/,
      use: [{
        loader: 'html-loader'
      }]
    }, {
      test: /\.scss$/,
      use: ExtractTextPlugin.extract({
        use: ['css-loader', 'sass-loader']
      })
    }]
  },

  plugins: [

    new HtmlWebpackPlugin({
      template: './index.html',
      chunksSortMode: 'dependency'
    }),
    new ExtractTextPlugin('style.css')
    // new webpack.optimize.CommonsChunkPlugin({
    //   name: ['vendor'].reverse()
    // })
  ]

};