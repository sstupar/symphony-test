import angular from 'angular';
import uiRouter from '@uirouter/angularjs';
import AppComponent from './app.component';

import SignupComponent from './components/signup/signup.module';
import SigninComponent from './components/signin/signin';
import DashboardComponent from './components/dashboard/dashboard';
import DetailsComponent from './components/details/details';
import HotelListComponent from './components/hotel-list/hotel-list';
import ServerService from './services/server-service/server.module';
import LocalstorageService from './services/local-storage-service/localstorage.module';

angular
  .module('app', [
    uiRouter,       
    SignupComponent,
    SigninComponent.name,
    ServerService.name,
    LocalstorageService.name,
    DashboardComponent.name,
    HotelListComponent.name,
    DetailsComponent.name
  ])
  .config(($locationProvider, $stateProvider, $urlRouterProvider) => {
    'ngInject';

    $stateProvider
      .state('app', {        
        abstract: true,
        template: '<app></app>'
      })
      .state('app.dashboard', {
        url: '/dashboard',
        template: '<dashboard></dashboard>'       
      })
      .state('app.signin', {
        url: '/signin',
        template: '<signin></signin>'
      })
      .state('app.details', {
        url: '/details',
        template: '<details></details>'
      })
      .state('app.signup', {
        url: '/signup',
        template: '<signup></signup>'
      });
    $urlRouterProvider.otherwise('/signup');
  })
  .component('app', AppComponent);