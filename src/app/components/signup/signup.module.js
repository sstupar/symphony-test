import angular from 'angular';
import signupComponent from './signup.component';
import childComponent from './child/child.component';

const signupModule = angular.module('signup', [])
  .component('signup', signupComponent)
  .component('child', childComponent);

export default signupModule.name;