import template from './child.html';
import './child.scss';
import controller from './child.controller';

const childComponent = {  
  bindings: {},
  template,
  controller
};

export default childComponent;
