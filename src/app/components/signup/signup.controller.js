class SignupComponentController {
  constructor($http, $state, ServerService) {
    'ngInject';
    this.username = '';
    this.password = '';
    this.email = '';
    this.firstName = '';
    this.lastName = '';
    this.$http = $http;
    this.$state = $state;    
    this.ServerService = ServerService;    
  }

  /**
   * Registers user on server.
   */
  signUp() {
    this.ServerService.signUp({
      username: this.username,
      password: this.password,
      email: this.email,
      first_name: this.firstName,
      last_name: this.lastName
    }).then((response) => {      
      if (response.status = 201) {
        this.$state.transitionTo('app.signin');
      }
    });
  }
}

export default SignupComponentController;