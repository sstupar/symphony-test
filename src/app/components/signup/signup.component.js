import template from './signup.html';
import './signup.scss';
import controller from './signup.controller';

const signupComponent = {  
  bindings: {},
  template,
  controller
};

export default signupComponent;
