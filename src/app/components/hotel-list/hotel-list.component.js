import template from './hotel-list.html';
import './hotel-list.scss';
import controller from './hotel-list.controller';

let hotelListComponent = {  
  bindings: {
    hotels: '='
  },
  template,
  controller
};

export default hotelListComponent;
