import angular from 'angular';
import hotelListComponent from './hotel-list.component';

let hotelListModule = angular.module('hotelList', [])

  .component('hotelList', hotelListComponent);

export default hotelListModule;