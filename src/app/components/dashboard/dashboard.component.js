import template from './dashboard.html';
import './dashboard.scss';
import controller from './dashboard.controller';

let signinComponent = {  
  bindings: {},
  template,
  controller
};

export default signinComponent;
