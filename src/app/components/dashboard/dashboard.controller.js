class dashboardComponentController {
  constructor($http, ServerService, $window) {
    this.$http = $http;
    this.hotelList = [],
    this.ServerService = ServerService;    
    this.$window = $window;
  }

  /**
   * Gets a list of hotels
   */
  loadHotels() {    
    this.ServerService.getHotels().then((response) => {
      this.hotelList = response.data;      
    });
  }

  /**
   * Loads reviews for specific hotel item.
   * @param {number} id 
   */
  showReview(id) {
    this.ServerService.getReview(id).then((response) => {
      console.log(response);
      this.hotelList.map((hotelItem) => {
        if (hotelItem.id === id) {
          hotelItem.reviews = response.data;
        }
      });
    });
  }

  /**
   * Opens new window with specific hotel details.
   * @param {number} id 
   */
  openNewWindow(id) {    
    this.$window.open(this.$window.location.origin + `#!/details?id=${id}`, '_blank', 'width=500, height=600');
  }
}

export default dashboardComponentController;