import template from './signin.html';
import './signin.scss';
import controller from './signin.controller';

let signinComponent = {  
  bindings: {},
  template,
  controller
};

export default signinComponent;
