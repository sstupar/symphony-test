class SigninComponentController {
  constructor($http, $state, ServerService, LocalstorageService) {
    'ngInject';
    this.username = '';
    this.password = '';
    this.$http = $http;
    this.$state = $state;
    this.ServerService = ServerService;
    this.LocalstorageService = LocalstorageService;
  }
  /**
   * Saves user data to localStorage.
   * @param {Object} response - response from user sign in
   */
  saveUser(response) {
    this.LocalstorageService.set(response.data.username, response.data);
    this.LocalstorageService.set('token', response.data.token);
  }

  /**
   * Signs in user.
   */
  signIn() {
    this.ServerService.signIn({
      username: this.username,
      password: this.password
    }).then((response) => {
      console.log(response);
      this.saveUser(response);
      if (response.status === 200) {
        this.$state.transitionTo('app.dashboard');
      }

    });
  }
}

export default SigninComponentController;