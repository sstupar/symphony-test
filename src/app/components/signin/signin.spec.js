import * as angular from 'angular';
import 'angular-mocks';

import Ctrl from './signin.controller';
let ServerService;

beforeEach(function () {
  angular.mock.module('app.service.server', function ($provide) {
    $provide.value('ServerService', {
      signIn: function () {
        return null;
      }
    });
  });

  inject(function (_ServerService_) {
    ServerService = _ServerService_;
  });

});

describe('sign in', function () {
  it('shoud do something', function () {
    let vm = new Ctrl();

    spyOn(ServerService, 'signIn');
    vm.signIn();
    expect(true).toBe(true);
  });
});