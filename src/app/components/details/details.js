import angular from 'angular';
import detailsComponent from './details.component';

let detailsModule = angular.module('details', [])

  .component('details', detailsComponent);

export default detailsModule;