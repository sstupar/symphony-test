import template from './details.html';
import './details.scss';
import controller from './details.controller';

let detailsComponent = {  
  bindings: {},
  template,
  controller
};

export default detailsComponent;
