class detailsComponentController {
  constructor($http, ServerService, $window) {
    this.$http = $http;
    this.ServerService = ServerService;
    this.$window = $window;
    this.hotelDetails = '';
  }

  $onInit() {
    // fast solution :), need to be replaced with proper url parsing.
    let id = this.$window.location.hash.split('=')[1];

    this.ServerService.getHotelDetails(id).then((response)=>{
      this.hotelDetails = response;
    });
  }

}

export default detailsComponentController;