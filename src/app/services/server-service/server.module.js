import ServerService from './server.service';

export default angular
  .module('app.service.server', [])
  .service('ServerService', ServerService);