export default class ServerService {
  constructor($http, LocalstorageService) {
    this.$http = $http;
    this.LocalstorageService = LocalstorageService;
  }

  /**
   * Registers user on server.
   * @param {Object} opts - option for registering user.
   *  username: '',
      password: '',
      email: '',
      first_name: '',
      last_name: ''
   * @returns {Promise}
   */
  signUp(opts) {
    return this.$http.post('http://localhost:8000/register/', opts);
  }

  /**
   * Signs in user.
   * @param {Object} opts - option for user sign in.
   *  username: '',
      password: '',
   * @returns {Promise}
   */
  signIn(opts) {
    return this.$http.post('http://localhost:8000/api-token-auth/', opts);
  }

  /**
   * Gets list of hotels.
   * @returns {Promise}
   */
  getHotels() {
    return this.$http.get('http://localhost:8000/hotel_api', {
      headers: {
        Authorization: `Token ${this.LocalstorageService.get('token')}`
      }
    });
  }

  /**
   * Creates request header with auth token.
   */
  getAuthRequestHeader() {
    return {
      headers: {
        Authorization: `Token ${this.LocalstorageService.get('token')}`
      }
    };
  }

  /**
   * Gets specific hotel details.
   * @param {string} id 
   */
  getHotelDetails(id) {
    return this.$http.get(`http://localhost:8000/hotel_api/${id}`, this.getAuthRequestHeader());
  }

  /**
   * Gets review for specific hotel.
   * @param {string} id - id of hotel.
   * @returns {Promise}
   */
  getReview(id) {
    return this.$http.get(`http://localhost:8000/hotel_api/get_hotel_reviews/${id}`, this.getAuthRequestHeader());
  }

}