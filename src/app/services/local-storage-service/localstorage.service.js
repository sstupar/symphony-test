export default class LocalstorageService {
  constructor() {

  }

  /**
   * Set value to local storage
   * @param {string} key
   * @param {object} value
   */
  set(key, value) {
    localStorage.setItem(this.prefixKey(key), JSON.stringify(value));
  }

  /**
   * Get value from local storage
   * @param {string} key
   * @returns {Object|Array|string|number|*}
   */
  get(key) {
    let value = localStorage.getItem(this.prefixKey(key));

    return value ? JSON.parse(value) : undefined;
  }

  getAuthToken() {
    return this.get(prefixKey('token'));
  }

  prefixKey(key) {
    return `symphony_${key}`;
  }

}