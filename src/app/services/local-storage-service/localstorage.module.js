import LocalstorageService from './localstorage.service';

export default angular
  .module('app.service.localStorage', [])
  .service('LocalstorageService', LocalstorageService);